//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RezerwacjaBiletow
{
    using System;
    using System.Collections.Generic;
    
    public partial class Wydarzenia
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Wydarzenia()
        {
            this.Bileties = new HashSet<Bilety>();
        }
    
        public string ID_Wydarzenia { get; set; }
        public string Nazwa_Wydarzenia { get; set; }
        public string Termin_Wydarzenia { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bilety> Bileties { get; set; }
    }
}
