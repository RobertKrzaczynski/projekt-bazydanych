//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RezerwacjaBiletow
{
    using System;
    using System.Collections.Generic;
    
    public partial class Dane_Klienta
    {
        public string IDKlienta { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public int Rocznik { get; set; }
        public string E_mail { get; set; }
        public string Telefon { get; set; }
        public string id_biletu { get; set; }
        public string id_rabatu { get; set; }
    
        public virtual Bilety Bilety { get; set; }
        public virtual Rabaty Rabaty { get; set; }
    }
}
